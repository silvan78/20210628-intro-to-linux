#!/bin/bash

MSG=("Warning up engines" "Pumping fuel" "Sparkling sparkplugs on" "Hyperglycol spray smoking" "Liftoff" "Stage 1 drop" "Stage 2 ignition" "MaxQ reached" "Coasting to atmosphere" "Stage 2 released" "Hydrolox engine ignition" "Heading prograde for apoapsis" "Apoapsis reached" "Circularizing orbit" "Reaching phase angle" "Performing Hochmann transfer to Minmus" "Leaving Kerbin orbit" "Entering Minimus SOF" "Insertion burn" "Retrograde landing burn" "Killing H-velocity" "Performing suicide burn" "Touchdown")

STIME=$(date +%s)

for line in $(seq 1 22)
 do

 sleep $((1 + RANDOM % 10))
 NTIME=$(date +%s)

 printf "[%4ss] %s\n" "+$((NTIME - STIME))" "${MSG[$line]}"
 
 done

echo "Congratulations, You have landed on Minmus, have a piece of green cheese."
sleep 20
echo -e "...\nDamm, Kebin, We have a problem; no dV to return; send Valentina with fuel, over.\n..."
